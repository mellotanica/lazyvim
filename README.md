# 💤 LazyVim

My custom [LazyVim](https://lazyvim.github.io/) configuration.

## Philosophy

This configuration is (obviously) tailored on my work habits, but tries to remain as close to classic vim behaviors and concepts as possible.

E.g. `s` in normal mode triggers characters substitution, line switching shortcuts are disabled.

Since I came from Spacemacs, which has a comprehensive and very helpful shortcuts palette, I am trying to keep whichkey updated with all shortcuts that I find useful even if coming from not fully supported plugins.

I am trying to use as much extras as I can to avoid duplicating plugin groups and configs already handled by the community.

## Most relevant key bindings

For most keybindings refer to [lazyvim keymas](https://www.lazyvim.org/keymaps#noicenvim), my most relevant overrides/additions are the following:

- insert mode
    - `<C-Space>`: show autocompletion popup
    - `<Tab>`: if completing a snippet/function, cycle through arguments
    - `.,` or `,.`: enter normal mode

- normal mode
    - `gd`: go to definition (symbol under cursor)
    - `gr`: search symbol references
    - `K`: show symbol documentation
    - `<leader>cc`: toggle comment on current line
    - ``` ` ```: open neotree navigator
    - `:S`: search (and replace) with smart case
    - `<leader><Space>`: fuzzy find files in current root
    - `<leader>/`: search (ripgrep) in current root
    - `<C-m>`: split horizontally
    - `<C-n>`: split vertically
    - `<C-q>`: close current buffer
    - `<leader>P`: open clipboard history selection window
    - `cr`: change current word case
    - `<C-s>`: enter flash mode, type a string to search it in current viewport with jump tags
    - `<leader>fP`: add current root to project list
    - `<leader>fp`: show projects list
    - `<leader>fh`: search files in home directory
    - `<leader>cv`: select virtualenv (while editing a python file)

    - `<leader>tn`, `<C-L>`: next tab
    - `<leader>tp`, `<C-H>`: prev tab
    - `<leader>tt`: new tab
    - `<leader>td`: close tab
    - `<leader>tf`, `<C-K>`: fist tab
    - `<leader>tl`, `<C-J>`: last tab

    - <leader>se: fast replace word under cursor

    - <leader>pc: add current project to known list
    - <leader>pf: search files in current project
    - <leader>pl: open project in a new tab
    - <leader>pm: search a project and add to list
    - <leader>pp: search known project

- debugger (normal mode)
    - `<leader>ds`: single step
    - `<leader>df`: focus current execution frame

- visual mode
    - `<leader>cc`: toggle comment on current selection
    - `ga`: enter align mode on current selection

    - <leader>se: fast replace selected text

- neotree (normal mode)
    - `<backspace>`: move root up one level (go to parent)
    - `a`: add new file/directory
    - `y`: copy current selection
    - `d`: delete current selection
    - `<CR>`: open current selection
    - `s`: open current selection in vertical split
    - `S`: open current selection in horizontal split
    - `h`: close current node
    - `l`: expand current node


## Plugin notes
### vim-abolish
[Abolish](https://github.com/tpope/vim-abolish) is a great plugin that offers both sensible substitution with `:S` (supporting regular `:s` flags) and quick case switch with coercion.
Substitution ignores case by default in search operation and tires to replicate input word case during replacement.

### venv-selector
Once a virtualenv is enabled for a specific project, the provided auto command automatically enables last used venv on file open.
Requires [lang.python](https://www.lazyvim.org/extras/lang/python) extra enabled.

### flash

Although being a nice navigation tool, the default binding is quite in the way if you are used to default vim bindings, so flash `s` key is remapped to `<C-s>`.

Also search integration feels wrong to me, when typing a search pattern I tend to type the whole word(s) I'm searching. Flash search integration tries to suggest jump points in active buffer that are constantly enabled while entering search pattern, so the first character in the search string that has no match in the buffer causes one of two actions (both of which are at least annoying to me):
- jump to the corresponding hit
- cancel search if no hump target was assigned the character I just typed
The following key types are handled as normal-mode commands so weird stuff happened to me each time I searched for uncertain sequences in documents.

### leap

Disabled altogether since flash is more than enough for my visual jumping needs and leap conflicted with character replace as well.

### mini-comment

I loved NerdCommenter back in the day, and the `<leader>cc` shortcut for commenting is stuck in my brain.

### specs
Nice cursor jump highlight plugin, not much more to say.

### yanky
Great clipboard integration plugin, provides a nice paste history selector, it only needs to be configured to use system clipboard registry by default.

### whichkey

Intermediate menu hints need to be registered in plugin configuration right now, so all keybinding groups coming from old or unsupported plugins will be described by overriding whichkey configs.
