return {
    "nvimdev/dashboard-nvim",
    optional = true,
    opts = function(_, opts)
        -- edit existing bindings
        local edit_bindings = {
            {
                desc = "Chezmoi",
                icon = "🏠",
                key = "c",
                replace_key = "C",
            },
        }

        -- define new bindings
        local new_bindings = {
            {
                action = "lua LazyVim.lazygit()",
                desc = "LazyGit",
                icon = "󰊢 ",
                key = "g",
                replace_key = "/",
                wanted_pos = 4,
            },
            {
                action = "lua LazyVim.pick.config_files()()",
                desc = "Config",
                icon = " ",
                key = "c",
                wanted_pos = 5,
            },
        }

        local function findEl(tbl, key)
            for i, item in ipairs(tbl) do
                if item.key == key then
                    return i
                end
            end
            return -1
        end

        local function fixBindigDesc(binding)
            return " " .. binding.desc .. string.rep(" ", 43 - #binding.desc)
        end

        for _, binding in ipairs(edit_bindings) do
            local edit_pos = findEl(opts.config.center, binding.key)
            if edit_pos >= 0 then
                opts.config.center[edit_pos].key = binding.replace_key
                opts.config.center[edit_pos].desc = fixBindigDesc(binding)
                opts.config.center[edit_pos].icon = binding.icon
            end
        end

        for _, binding in ipairs(new_bindings) do
            binding.desc = fixBindigDesc(binding)
            binding.key_format = "  %s"
            local key_pos = findEl(opts.config.center, binding.key)
            local target_pos = binding.wanted_pos
            if key_pos >= 0 then
                opts.config.center[key_pos].key = binding.replace_key
                target_pos = key_pos + 1
            end
            table.insert(opts.config.center, target_pos, binding)
        end
    end,
}
