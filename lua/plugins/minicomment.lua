return {
    "echasnovski/mini.comment",
    opts = {
        mappings = {
            comment_line = '<leader>cc',
            comment_visual = '<leader>cc',
        },
    },
}
