return {
    {
        "echasnovski/mini.align",
        event = "VeryLazy",
        opts = {
            mappings = {
                start = '',
                start_with_preview = 'ga',
            },
        },
        config = function(_, opts)
            require("mini.align").setup(opts)
        end,
    },
}
