return {
    "mfussenegger/nvim-dap",
    keys = {
        { "<leader>ds", "<cmd>lua require('dap').step_over()<cr>", desc = "Step Over" },
        { "<leader>df", "<cmd>lua require('dap').focus_frame()<cr>", desc = "Focus current frame" },
        { "<leader>dS", "<cmd>lua require('dap').session()<cr>", desc = "Session" },
        { "<leader>dO", false },
    },
}
