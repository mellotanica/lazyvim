return {
    {
        "echasnovski/mini.indentscope",
        init = function()
            vim.api.nvim_create_autocmd("FileType", {
                pattern = { "nvcheatsheet" },
                callback = function()
                    vim.b.miniindentscope_disable = true
                end,
            })
        end,
    },
    {
        "kingavatar/nvchad-ui.nvim",
        branch = "v2.0",
        lazy = false,
        config = function()
            require("nvchad_ui").setup({
                lazyVim = true,
                statusline = {enabled = false},
                tabufline = {enabled = false},
                nvdash = {load_on_startup = false},
                lsp = {signature = {enabled = false}}
            })
        end,
    },
}
