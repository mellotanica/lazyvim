return {
    {
        "williamboman/mason.nvim",
        opts = {
            ensure_installed = {
        -- shell
                "beautysh",
                "shellcheck",
                "bash-language-server",
        -- xml
                "lemminx",
                "xmlformatter",
        -- arduino
                "arduino-language-server",
            },
        },
    },
}
