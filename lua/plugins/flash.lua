return {
    "folke/flash.nvim",
    keys = {
        { "s", mode = { "n", "x", "o" }, false },
        {
            "<c-s>",
            mode = { "n", "x", "o" },
            function()
                require("flash").jump()
            end,
            desc = "Flash",
        },
    },
    opts = {
        modes = {
            search = {
                enabled = false,
            },
        },
    },
}
