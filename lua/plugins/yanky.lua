return {
    "gbprod/yanky.nvim",
    opts = {
        picker = {
            select = {
                action = require("yanky.picker").actions.set_register('+')
            },
        },
        ring = {
            update_register_on_cycle = true,
        },
    },
}
