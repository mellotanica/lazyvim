return {
    {
        "nvim-treesitter/nvim-treesitter",
        opts = { ensure_installed = { "hyprlang" } },
    } , {
        "williamboman/mason.nvim",
        opts = { ensure_installed = { "hyprls" } },
    } , {
        "neovim/nvim-lspconfig",
        opts = { servers = { hyprls = {
            filetypes = { "*.hl", "hypr*.conf", "hypr/*.conf", "hyprlang" }
        } } }
    }
}

