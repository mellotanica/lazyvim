return {
    {
        "folke/which-key.nvim",
        opts = {
            preset = "helix",
            spec = {
                { "cr", group = "Coerce" },
            },
        },
    },
}
