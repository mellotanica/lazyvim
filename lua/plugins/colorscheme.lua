return {
    {
        "catppuccin/nvim",
        lazy = true,
        name = "catppuccin",
        opts = {
            flavour = "latte",
            color_overrides = {
                latte = {
                    base = "#ffffff",
                    mauve = "#56b4e9",
                    blue = "#d55e00",
                },
            },
        },
    },
    {
        "LazyVim/LazyVim",
        opts = function()
            local COLORTERM = os.getenv("COLORTERM")
            local TERM = os.getenv("TERM")
            if not (COLORTERM == "truecolor" or TERM == "xterm-256color") then
                return {colorscheme = "tokyonight"}
            else
                return {colorscheme = "catppuccin"}
            end
        end,
    },
}
