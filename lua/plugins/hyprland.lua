return {
    "nvim-treesitter/nvim-treesitter",
    opts = function(_, opts)
        table.insert(opts.ensure_installed, "hyprlang")
    end,
    conf = function(_, opts)
        vim.filetype.add({
            pattern = {
                [".*/hypr/.*%.conf"] = "hyprlang",
                [".*/hyprland%.conf"] = "hyprlang",
            },
        })
        require('nvim-treesitter').setup(opts)
    end,
}
