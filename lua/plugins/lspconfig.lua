return {
    "neovim/nvim-lspconfig",
    opts = function()
        local keys = require("lazyvim.plugins.lsp.keymaps").get()
        keys[#keys + 1] = { "<leader>cc", mode = { "n", "v" }, false }
        keys[#keys + 1] = { "<leader>cL", mode = { "n", "v" }, vim.lsp.codelens.run, desc = "Run codelens" }
    end,
}
