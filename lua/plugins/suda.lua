return {
    {
        "lambdalisue/suda.vim",
        lazy = false,
        init = function(_)
            vim.cmd("cabbrev W SudaWrite")
            vim.cmd("cabbrev R SudaRead")
            vim.g.suda_smart_edit = true
        end,
    },
}
