return {
    {
        "akinsho/bufferline.nvim",
        opts = {
            options = {
                always_show_bufferline = true,
            },
        },
    },
    {
        "tiagovla/scope.nvim",
        config = function()
            require("scope").setup()
        end,
    },
}
