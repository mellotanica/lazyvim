return {
    "tpope/vim-abolish",
    lazy = false,
    keys = {
        { "crc", "<Plug>(abolish-coerce-word)c", desc = "camelCase" },
        { "crs", "<Plug>(abolish-coerce-word)s", desc = "snake_case" },
        { "cr_", "<Plug>(abolish-coerce-word)_", desc = "snake_case" },
        { "crm", "<Plug>(abolish-coerce-word)m", desc = "MixedCase" },
        { "crp", "<Plug>(abolish-coerce-word)p", desc = "PascalCase (aka MixedCase)" },
        { "cru", "<Plug>(abolish-coerce-word)u", desc = "SNAKE_UPPERCASE" },
        { "crU", "<Plug>(abolish-coerce-word)U", desc = "SNAKE_UPPERCASE" },
        { "crk", "<Plug>(abolish-coerce-word)k", desc = "kebab-case (irreversible)" },
        { "cr-", "<Plug>(abolish-coerce-word)-", desc = "dash-case (aka kebab-case, irreversible)" },
        { "cr.", "<Plug>(abolish-coerce-word).", desc = "dot.case (irreversible)" },
    },
}
