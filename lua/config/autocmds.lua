-- Autocmds are automatically loaded on the VeryLazy event
-- Default autocmds that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/autocmds.lua
-- Add any additional autocmds here

-- try to automatically load virtualenv for python projects
-- vim.api.nvim_create_autocmd('VimEnter', {
--   desc = 'Auto select virtualenv Nvim open',
--   pattern = '*',
--   callback = function()
--     local venv = vim.fn.finddir('venv', vim.fn.getcwd() .. ';')
--     if venv ~= '' then
--       require('venv-selector').retrieve_from_cache()
--     end
--   end,
--   once = true,
-- })

vim.api.nvim_create_autocmd({ "FileType" }, {
  pattern = { "go" },
  callback = function()
    vim.b.autoformat = true
  end,
})


vim.api.nvim_create_autocmd({ "FileType" }, {
  pattern = { "gdscript" },
  callback = function()
    vim.cmd('set noet')
  end,
})
