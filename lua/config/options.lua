-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

vim.opt.relativenumber = false
vim.opt.wrap = true
vim.g.autoformat = false
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.listchars = {
    trail = "-",
    nbsp = "+",
    tab = "  ",
}
vim.g.root_spec = { { ".git", ".svn" }, "lsp", { ".scuba.yml", "CMakeLists.txt" }, "cwd" }
vim.opt.spelllang = {"en", "it"}

--- force osc 52 to handle clipboard to use a platform indepentend approach
local function paste()
    return { vim.fn.split(vim.fn.getreg(""), "\n"), vim.fn.getregtype("") }
end

vim.g.clipboard = {
    name = "OSC 52",
    copy = {
        ["+"] = require("vim.ui.clipboard.osc52").copy("+"),
        ["*"] = require("vim.ui.clipboard.osc52").copy("*"),
    },
    paste = {
        ["+"] = paste,
        ["*"] = paste,
    },
}

--- use system clipboard by default to ensure proper osc 52 integration is used all the time
vim.opt.clipboard = "unnamedplus"
