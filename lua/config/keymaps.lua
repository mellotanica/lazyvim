-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

-- alternate esc (avoid jk because it triggers weird behaviors in visual mode when holding key)
vim.keymap.set("i", ".,", "<Esc>")
vim.keymap.set("i", ",.", "<Esc>")

-- disable move line shortcuts to preserve mental health
vim.keymap.del({ "n", "v", "i" }, "<a-j>")
vim.keymap.del({ "n", "v", "i" }, "<a-k>")

-- window split
vim.keymap.set("n", "<c-m>", "<cmd>split<cr>", { desc = "split horizontally" })
vim.keymap.set("n", "<c-n>", "<cmd>vsplit<cr>", { desc = "split vertically" })
vim.keymap.set("n", "<c-q>", "<cmd>bdelete<cr>", { desc = "close buffer" })

-- tabs handling
vim.keymap.del("n", "<leader><Tab>[")
vim.keymap.del("n", "<leader><Tab>]")
vim.keymap.del("n", "<leader><Tab>d")
vim.keymap.del("n", "<leader><Tab><Tab>")
vim.keymap.del("n", "<leader><Tab>f")
vim.keymap.del("n", "<leader><Tab>l")

vim.keymap.set("n", "<leader>tn", "<cmd>tabnext<cr>", { desc = "Next tab" })
vim.keymap.set("n", "<leader>tp", "<cmd>tabprev<cr>", { desc = "Previous tab" })
vim.keymap.set("n", "<leader>th", "<cmd>tabnext<cr>", { desc = "Next tab" })
vim.keymap.set("n", "<leader>tl", "<cmd>tabprev<cr>", { desc = "Previous tab" })
vim.keymap.set("n", "<leader>tt", "<cmd>tabnew<cr>", { desc = "New tab" })
vim.keymap.set("n", "<leader>td", "<cmd>tabclose<cr>", { desc = "Close tab" })
vim.keymap.set("n", "<leader>tgg", "<cmd>tabfirst<cr>", { desc = "First tab" })
vim.keymap.set("n", "<leader>tG", "<cmd>tablast<cr>", { desc = "Last tab" })
vim.keymap.set("n", "<leader>tk", "<cmd>tabfirst<cr>", { desc = "First tab" })
vim.keymap.set("n", "<leader>tj", "<cmd>tablast<cr>", { desc = "Last tab" })

vim.keymap.set("n", "<C-S-l>", "<cmd>tabnext<cr>", { desc = "Next tab" })
vim.keymap.set("n", "<C-S-h>", "<cmd>tabprev<cr>", { desc = "Previous tab" })
vim.keymap.set("n", "<C-S-k>", "<cmd>tabfirst<cr>", { desc = "New tab" })
vim.keymap.set("n", "<C-S-j>", "<cmd>tablast<cr>", { desc = "Close tab" })

-- neotree, match open shorcut to neotree close when focused
vim.keymap.set(
    { "n" },
    "`",
    "<cmd>lua require('neo-tree.command').execute({ toggle = true, dir = LazyVim.root() })<cr>",
    { desc = "Toggle NeoTree" }
)

-- better replace shortcut
vim.keymap.set(
    "n",
    "<Space>se",
    ":%s/<C-r><C-w>//gc<Left><Left><Left>",
    { desc = "Replace word under cursor in current buffer" }
)
vim.keymap.set(
    "v",
    "<Space>se",
    '"zye:%s/<C-r>z//gc<Left><Left><Left>',
    { desc = "Replace selected word in current buffer" }
)
vim.keymap.set("n", "<Space>sE", ":%s///gc<Left><Left><Left><Left>", { desc = "Replace word in current buffer" })
